<?php
/**
 * Causality Tools
 *
 * Plugin Name: Causality Tools
 * Description: Common WP functionality across all Causality projects
 * Author:      Causality Agency
 * License:     Proprietary
 * Text Domain: causality-tools
 * Domain Path: /languages
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Invalid request.' );
}

include_once(__DIR__ . DIRECTORY_SEPARATOR . 'inc' . DIRECTORY_SEPARATOR . 'CA_Tools_Manage_Notices.php');
(new CA_Tools_Manage_Notices())->init();

include_once(__DIR__ . DIRECTORY_SEPARATOR . 'inc' . DIRECTORY_SEPARATOR . 'CA_Tools_Toolbar_Version.php');
(new CA_Tools_Toolbar_Version())->init();
