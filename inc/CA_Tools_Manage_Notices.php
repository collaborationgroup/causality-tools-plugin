<?php

/**
 * Class CA_Tools_Manage_Notices
 *
 * Manages the types of notifications that WP admins get
 */
class CA_Tools_Manage_Notices {

	public function init() {
		$this->remove_core_update_notifications();
		$this->remove_missleading_health_chacks();
	}

	/**
	 * Don't encourage admins to upgrade since updates are done with composer
	 */
	private function remove_core_update_notifications() {
		add_action( 'admin_head', function () {
			remove_action( 'admin_notices', 'update_nag', 3 );
			remove_action( 'admin_notices', 'maintenance_nag', 10 );
			remove_action( 'network_admin_notices', 'maintenance_nag', 10 );
		} );
	}

	/**
	 * Remove health checks that are misleading for a dockerized setup
	 *
	 * @see: https://github.com/WordPress/WordPress/blob/5.2/wp-admin/includes/class-wp-site-health.php#L1726-L1846
	 */
	private function remove_missleading_health_chacks() {
		add_filter( 'site_status_tests', function ( $tests ) {
			unset( $tests['direct']['wordpress_version'] );
			unset( $tests['direct']['plugin_version'] );
			unset( $tests['direct']['php_extensions'] );
			unset( $tests['async']['background_updates'] );

			return $tests;
		} );
	}
}
