<?php

class CA_Tools_Toolbar_Version {

	const TIMEZONE = 'Europe/Stockholm';

	public function init() {
		add_action( 'admin_bar_menu', [ $this, 'run' ], 999 );

		// We use a GET, since we need to intercept this action even if parts of WordPress breaks
		if ( isset( $_GET["ca-tools-action"] ) && $_GET["ca-tools-action"] === "clear-apcu" ) {
			$this->clear_apcu();
			exit;
		}
	}

	public function run( \WP_Admin_Bar $bar ) {
		try {
			$this->add_causality_toolbar( $bar );
		} catch ( \Throwable $e ) {
			// Don't ever let the plugin brake the site
		}
	}

	/**
	 * Adds an admin navbar item to staging and production environments
	 *
	 * Displays:
	 * Git hash
	 * Build time
	 * Theme version
	 * Latest commit message
	 * Button to clear ACPu cache
	 *
	 * @param WP_Admin_Bar $bar
	 */
	private function add_causality_toolbar( \WP_Admin_Bar $bar ) {
		$theme = wp_get_theme();
		$path  = ABSPATH . '../version.txt';

		if ( ! file_exists( $path ) ) {
			return;
		}

		list( $datetime, $commit, $description ) = file( $path );

		// Add commit hash
		$bar->add_menu( [
			'id'    => 'casuality-tools-version',
			'title' => '<span class="ab-icon dashicons dashicons-admin-generic"></span>' . $commit,
		] );

		// Add build time
		$bar->add_menu( [
			'id'     => 'casuality-tools-version-row-1',
			'parent' => 'casuality-tools-version',
			'title'  => 'Built at: ' . $this->to_local_time( $datetime ),
		] );

		// Add theme version
		$bar->add_menu( [
			'id'     => 'casuality-tools-version-row-2',
			'parent' => 'casuality-tools-version',
			'title'  => 'Theme version: ' . $theme->get( 'Version' ),
		] );

		// Add commit message
		$bar->add_menu( [
			'id'     => 'casuality-tools-version-row-3',
			'parent' => 'casuality-tools-version',
			'title'  => strlen( $description ) > 50 ? substr( $description, 0, 50 ) . '...' : $description,
		] );

		// Clear OPCache
		$bar->add_menu( [
			'id'     => 'casuality-tools-version-row-4',
			'parent' => 'casuality-tools-version',
			'title'  => 'Clear APCu cache',
			'meta'   => [ 'onclick' => 'jQuery.get("/?ca-tools-action=clear-apcu", function(e){alert(e)})' ],
		] );

	}

	private function clear_apcu() {
		try {
			apcu_clear_cache();
			echo "Cache was cleared!";
		} catch ( \Throwable $e ) {
			echo "An error occured while clearing cache";
		}
	}

	private function to_local_time( string $time ): string {
		date_default_timezone_set( 'UTC' );
		$new_date = new DateTime( $time );
		$new_date->setTimeZone( new DateTimeZone( self::TIMEZONE ) );

		return $new_date->format( "Y-m-d H:i" );
	}
}
